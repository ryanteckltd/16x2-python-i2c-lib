#! /usr/bin/env python

import lcddriver
import time
import sys

# You can specify the i2c address to the driver
# The default is 0x27, the 16x4 LCD uses 0x3f
if len(sys.argv) > 1:
	lcd = lcddriver.lcd(int(sys.argv[1],16))
else:
	lcd = lcddriver.lcd()

# Turn the backlight on, writing text also turns it on
lcd.lcd_backlight(True)

# Write lines to the display
lcd.lcd_display_string("16X2 I2C Display", 1)
lcd.lcd_display_string("Buy@ Ryanteck.uk", 2)
time.sleep(2)

# Clear all text and write some more
lcd.lcd_clear()
lcd.lcd_display_string("Waiting to exit....", 1)

# Wait to exit
print("Press Enter/Return to exit")
x = raw_input()

# Turn off the blacklight before exit
lcd.lcd_backlight(False)
